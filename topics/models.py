"""
Created by Chen-Che Huang
Define Topic model
"""
import uuid

# Create your models here.


class Topic(object):
    """
    Topic model is made of a unique ID, user-created content, and associated upvotes and downvotes
    """
    def __init__(self, content=None):
        if content is None:
            raise ValueError('Topic content cannot be None')
        self.uuid = str(uuid.uuid4())
        self.content = content
        self.upvotes = 0
        self.downvotes = 0

    def get_id(self):
        return self.uuid

    def get_content(self):
        return self.content

    def get_upvotes(self):
        return self.upvotes

    def get_downvotes(self):
        return self.downvotes

    def increment_upvotes_by_one(self):
        self.upvotes += 1

    def decrement_upvotes_by_one(self):
        self.downvotes += 1

