from .models import Topic


class TopicRepo(object):
    """
    TopicRepo contains all the data structures for topics storage and efficient operations
    topic_repo: topic1, topic2, topic3, ..., topicN
    id_to_topic: uuid1: topic1, uuid2: topic2, uuid3: topic3
    order_to_id: 1: uuidk, 2:uuidm, 3: uuidq, ..., 20: uuidt where uuidk gets the
    most upvotes, next uuidm, next uuidq, and the 20th is uuidt
    """

    def __init__(self):
        self.topic_repo = list()
        self.id_to_topic = dict()
        self.order_to_id = dict()

        self.MAX_TOPIC_LENGTH = 255
        self.MAX_RETURNED_COUNT = 20
        self.topic_count = 0

    def create_topic(self, topic_content):
        if topic_content == '':
            raise ValueError('Empty topic is not allowed')
        if len(topic_content) > self.MAX_TOPIC_LENGTH:
            raise ValueError('Topic must contain no more than 255 characters')
        if not self._contains_by_topic_content(topic_content):
            topic = Topic(topic_content)
            self.topic_repo.append(topic)
            self.id_to_topic[topic.get_id()] = topic
            if self.topic_count <= self.MAX_RETURNED_COUNT:
                self.order_to_id[self.topic_count+1] = topic.get_id()
            self.topic_count += 1
        else:
            raise ValueError('The same topic has existed')

    def get_most_upvoted_topics(self):
        most_upvoted_topics = list()
        for order in sorted(self.order_to_id.keys()):
            most_upvoted_topics.append(self.id_to_topic[self.order_to_id[order]])
        return most_upvoted_topics

    def upvote_topic(self, topic_id):
        if not self._contains_by_topic_id(topic_id):
            raise ValueError(topic_id + ' not found')
        topic = self.id_to_topic[topic_id]
        topic.increment_upvotes_by_one()
        self._update_order_to_id(topic_id)

    def downvote_topic(self, topic_id):
        if not self._contains_by_topic_id(topic_id):
            raise ValueError(topic_id + ' not found')
        topic = self.id_to_topic[topic_id]
        topic.decrement_upvotes_by_one()

    def _update_order_to_id(self, topic_id):
        # TODO: implement binary search to improve performance
        if len(self.order_to_id) == 1:
            return
        elif len(self.order_to_id) == 2:
            if self.id_to_topic[self.order_to_id[2]].get_upvotes() > self.id_to_topic[self.order_to_id[1]].get_upvotes():
                self.order_to_id[2] = self.order_to_id[1]
                self.order_to_id[1] = topic_id
        else:
            if self.topic_count > self.MAX_RETURNED_COUNT:
                largest_order = self.MAX_RETURNED_COUNT
            else:
                largest_order = len(self.order_to_id) - 1
            current_topic_upvotes = self.id_to_topic[topic_id].get_upvotes()
            print('1: ' + str(largest_order))
            if current_topic_upvotes > self.id_to_topic[self.order_to_id[largest_order]].get_upvotes():
                print('2: ' + str(largest_order))
                for reverse_order in reversed(range(1, largest_order+1)):
                    print('3: ' + str(reverse_order))
                    if current_topic_upvotes <= self.id_to_topic[self.order_to_id[reverse_order]].get_upvotes():
                        print('x: ' + str(self.id_to_topic[self.order_to_id[reverse_order]].get_upvotes()))
                        for order in range(reverse_order+1, largest_order+1):
                            print('4: ' + str(order))
                            self.order_to_id[order+1] = self.order_to_id[order]
                        self.order_to_id[reverse_order+1] = topic_id
                        break

    def _contains_by_topic_id(self, topic_id):
        for topic in self.topic_repo:
            if topic_id == topic.get_id():
                return True
        return False

    def _contains_by_topic_content(self, topic_content):
        for topic in self.topic_repo:
            if topic_content == topic.get_content():
                return True
        return False
