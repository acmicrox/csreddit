from django.template import loader
from .repository import TopicRepo
from django.http import HttpResponseRedirect, HttpResponse
from django.urls import reverse
from django.http import HttpResponseServerError


topic_repo = TopicRepo()


def index(request):

    try:
        most_upvotes_topics = topic_repo.get_most_upvoted_topics()
        template = loader.get_template('topics/index.html')
        context = {
            'most_upvotes_topics': most_upvotes_topics,
        }
        return HttpResponse(template.render(context, request))
    except Exception as e:
        print(str(e))
        return HttpResponseServerError('Sorry! We are encountering an unknown error!')


def upvote(request, topic_id):
    try:
        topic_repo.upvote_topic(topic_id)
        return HttpResponseRedirect(reverse(index))
    except ValueError as e:
        return HttpResponse(str(e))
    except Exception as e:
        print(str(e))
        return HttpResponseServerError('Sorry! We are encountering an unknown error!')


def downvote(request, topic_id):
    try:
        topic_repo.downvote_topic(topic_id)
        return HttpResponseRedirect(reverse(index))
    except ValueError as e:
        return HttpResponse(str(e))
    except Exception as e:
        print(str(e))
        return HttpResponseServerError('Sorry! We are encountering an unknown error!')


def create(request):
    try:
        topic_repo.create_topic(request.POST['content'])
        return HttpResponseRedirect(reverse(index))
    except ValueError as e:
        return HttpResponse(str(e))
    except Exception as e:
        print(str(e))
        return HttpResponseServerError('Sorry! We are encountering an unknown error!')
