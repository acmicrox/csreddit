from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('create', views.create, name='create'),
    path('topics/<topic_id>/upvote/', views.upvote, name='upvote'),
    path('topics/<topic_id>/downvote/', views.downvote, name='downvote')
]
