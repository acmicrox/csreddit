from django.test import TestCase
from .repository import TopicRepo
from .models import Topic


class TopicRepoTests(TestCase):

    def test_add_topic_ok(self):
        topic_repo = TopicRepo()
        topic_repo.create_topic('test')
        self.assertEquals(1, topic_repo.get_number_of_topics())

    def test_add_empty_topic_throw_exception(self):
        topic_repo = TopicRepo()
        with self.assertRaisesMessage(ValueError, 'Empty topic is not allowed'):
            topic_repo.create_topic('')

    def test_add_over_long_topic_throw_exception(self):
        topic_repo = TopicRepo()
        with self.assertRaisesMessage(ValueError, 'Topic length exceed 255 characters'):
            max_top_length = 255
            over_long_topic = ['a' for _ in range(max_top_length+1)]
            topic_repo.create_topic(str(over_long_topic))

    def test_add_existing_topic_throw_exception(self):
        topic_repo = TopicRepo()
        with self.assertRaisesMessage(ValueError, 'The same topic has existed'):
            topic_repo.create_topic('test')
            topic_repo.create_topic('test')

    def test_return_at_most_20_topics(self):
        topic_repo = TopicRepo()
        for index in range(30):
            TopicRepo.create_topic(Topic(str(index)))
        topics = topic_repo.get_most_upvoted_topics()
        self.assertEquals(20, len(topics))

