# Reddit
This is a Reddit-like forum which allows user to post topics and upvote/downvote topics.

# How to host this forum on your compute
This project is created based on top of Django, a Python Web framework.
Please refer to Django document to install Django first (https://docs.djangoproject.com/en/2.0/intro/install/).
After Django installation, you can start this server locallly by running the following commands.
```
git clone https://acmicrox@bitbucket.org/acmicrox/csreddit.git
cd csreddit
python manage.py runserver
```
Then, you can start to use this by opening http://127.0.0.1:8000/ on your browser (all tests are done on Chrome,
but shall be work on all browsers)
